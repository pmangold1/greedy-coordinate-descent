import numpy as np
import pickle
import matplotlib.pyplot as plt
import seaborn as sns

palette = sns.color_palette("colorblind")

with open("optimals.pkl", "rb") as f:
    optimals = pickle.load(f)


for dataset, values in optimals.items():

    fig, ax = plt.subplots(1, 1, figsize=(4,3))

    coefs = np.sort(np.abs(values["coef"]))
    hist = ax.hist(coefs, alpha=0.6)[0]
    ax.set_yscale("log")


    quantile99 = np.quantile(coefs, 0.99)
    alpha = np.quantile(coefs, 1-5.0/len(coefs))

    print(dataset[2], ":", 1 - 5.0 / len(coefs), alpha)

    # ax.plot([quantile99] * 2, [0, np.max(hist) * 5],
    #         color=palette[2])
    ax.plot([alpha] * 2, [0, np.max(hist) * 5],
            lw=4,
            color=palette[4])


    # ax.plot([quantile99] * 5,
    #         np.logspace(-3, np.log10(np.max(hist)), 5),
    #         marker="*",
    #         color=palette[2])
    hdl, = ax.plot([alpha] * 5,
                   np.logspace(-3, np.log10(np.max(hist)), 5),
                   marker="x",
                   markersize=10, markeredgewidth=3,
                   lw=4,
                   color=palette[4])

    if dataset[2] == "madelon" and dataset[1] == "L1Regularizer":
        ax.legend([hdl],
                  [r"value of $\alpha$ for"  + "\n" + r"$(\alpha, 5)$-quasi-sparsity"],
                  loc="upper right", fontsize=15)

    dataset_name = dataset[2] + "_" + dataset[0] + "_" + dataset[1]
    fig.savefig("imbalance_plots/" + dataset_name + ".pdf", bbox_inches="tight")
