import numpy as np
import pickle
import matplotlib.pyplot as plt
import seaborn as sns

palette = sns.color_palette("colorblind")


def find_nearest(array,value):
    idx = (np.abs(array-value)).argmin()
    return idx



with open("optimals.pkl", "rb") as f:
    optimals = pickle.load(f)

with open("results/run_results.pkl", "rb") as f:
    results = pickle.load(f)

plots = {r : {} for r in results["cd"]}
print(plots)

print(results)
for algo in results:
    for dataset in results[algo]:
        curr = results[algo][dataset]

        x = curr[0].x_
        obj = np.array([
            c.obj_
            for c in curr
        ])
        time = np.mean([
            c.elapsed_time_
            for c in curr
        ], axis=0)

        plots[dataset][algo] = (np.array(x), np.array(time), np.array(obj))

markers = {"cd": "^", "sgd": "*", "gcd": "o",
           "gcd_gss": "P", "gcd_gsr": "X", "gcd_gsq": "D"}
color = { "cd": palette[0], "sgd": palette[1], "gcd": palette[2],
          "gcd_gss": palette[3], "gcd_gsr": palette[4], "gcd_gsq": palette[5] }


for dataset in plots:

    fig, ax = plt.subplots(1, 1, figsize=(4,3))
    fig_time, ax_time = plt.subplots(1, 1, figsize=(4,3))
    fig_gs, ax_gs = plt.subplots(1, 1, figsize=(4,3))

    opt = optimals[(dataset[1], dataset[2], dataset[0])]["obj"]

    max_x = np.max([np.max(plots[dataset][alg_][0]) for alg_ in plots[dataset]])
    max_time = np.max([np.max(plots[dataset][alg_][1]) for alg_ in plots[dataset]])

    for algo in ["cd", "sgd", "gcd", "gcd_gss", "gcd_gsr", "gcd_gsq"]:
        x, time, obj = plots[dataset][algo]
        obj_mean = (np.mean(obj, axis=0) - opt) / opt
        obj_min = (np.min(obj, axis=0) - opt) / opt
        obj_max = (np.max(obj, axis=0) - opt) / opt

        if algo in ["cd", "sgd", "gcd"]:
            ax.plot(x, obj_mean, lw=3, color=color[algo])
            ax_time.plot(time, obj_mean, lw=3, color=color[algo])

            idx = [find_nearest(x, tick) for tick in np.arange(max_x, step=max_x/5)] + [len(x)-1]

            hdl, = ax.plot(x[idx], obj_mean[idx], lw=0,
                           marker=markers[algo], markersize=15, markeredgewidth=3,
                           color=color[algo])


            ax_time.plot(time[idx], obj_mean[idx], lw=0,
                         marker=markers[algo], markersize=15, markeredgewidth=3,
                         color=color[algo])

            ax.fill_between(x, obj_min, obj_max, alpha=0.5, color=color[algo])
            ax_time.fill_between(time, obj_min, obj_max, alpha=0.5, color=color[algo])

        ax_gs.plot(x, obj_mean, lw=3, color=color[algo])

        idx = [find_nearest(x, tick) for tick in np.arange(max_x, step=max_x/5)] + [len(x)-1]

        ax_gs.plot(x[idx], obj_mean[idx], lw=0,
                   marker=markers[algo], markersize=15, markeredgewidth=3,
                   color=color[algo])

        ax_gs.plot(x, obj_mean, lw=3, color=color[algo])
        ax_gs.fill_between(x, obj_min, obj_max, alpha=0.5, color=color[algo])

#    ax.set_title(", ".join(dataset))

    if(max_x == 3):
        ax.set_xticks([0,1,2,3])

    ax.set_yscale("log")
    ax_time.set_yscale("log")
    ax_gs.set_yscale("log")

    ax.tick_params(axis='both', which='major', labelsize=15)
    ax.tick_params(axis='both', which='minor', labelsize=15)

    ax_time.tick_params(axis='both', which='major', labelsize=15)
    ax_time.tick_params(axis='both', which='minor', labelsize=15)

    ax.set_xlabel("Passes on data", fontsize=20)
    ax_time.set_xlabel("Time (s)", fontsize=20)


    hdls = [ ax.plot([0],[0],
                     lw=4,color=color[algo],
                     marker=markers[algo],markersize=15,markeredgewidth=3)[0]
             for algo in ["cd", "sgd", "gcd"] ]

    hdls_time = [ ax_time.plot([0],[0],
                               lw=4,color=color[algo],
                               marker=markers[algo],markersize=15,markeredgewidth=3)[0]
                  for algo in ["cd", "sgd", "gcd"] ]

    hdls_gs = [ ax_time.plot([0],[0],
                             lw=4,color=color[algo],
                             marker=markers[algo],markersize=15,markeredgewidth=3)[0]
                for algo in ["cd", "sgd", "gcd", "gcd_gss", "gcd_gsr", "gcd_gsq"] ]

    if(dataset[0] == "madelon" and dataset[2] == "L1Regularizer"):
        ax.legend(hdls, ["DP-CD", "DP-SGD", "DP-GCD"], fontsize=20)
        ax_time.legend(hdls_time, ["DP-CD", "DP-SGD", "DP-GCD"], fontsize=20)
        ax_gs.legend(hdls_gs, ["DP-CD", "DP-SGD", "DP-GCD",
                            "DP-GCD-GSs", "DP-GCD-GSr", "DP-GCD-GSq"], fontsize=15)


    fig.tight_layout()
    fig_time.tight_layout()

    fig.savefig("plots/" + "_".join(dataset) + ".pdf", bbox_inches="tight")
    fig_time.savefig("plots/" + "_".join(dataset) + "_time.pdf", bbox_inches="tight")
    fig_gs.savefig("plots/" + "_".join(dataset) + "_gs.pdf", bbox_inches="tight")
