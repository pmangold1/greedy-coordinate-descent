import numpy as np

from pcoptim import coordinate_descent, gradient_descent, greedy_coordinate_descent

algo_params_list = [
    (coordinate_descent,
     {
         "max_iter": [0.001, 0.01, 0.1, 1, 2, 3, 5, 10, 20],
         "learning_rate": np.logspace(-2, 1, 10),
         "clip": np.logspace(-4, 6, 50),
         "strategy": ["random"]
     }
     ),
    (gradient_descent,
     {
         "batch_size": [1],
         "batch_strategy": ["random"],
         "max_iter": [0.001, 0.01, 0.1, 1, 2, 3, 5, 10, 20],
         "learning_rate": np.logspace(-6, 0, 10),
         "clip": np.logspace(-4, 6, 50)
     }
     ),
    (greedy_coordinate_descent,
     {
         "epochs": [False],
         "max_iter": [1, 2, 4, 7, 10, 20], #, 30, 50],#, 100, 300],
         "learning_rate": np.logspace(-2, 1, 10),
         "strategy": ["GSs"],
         #    "rectify_bad_step": [True, False],
         "clip": np.logspace(-4, 6, 50)
     }
     ),
    (greedy_coordinate_descent,
     {
         "epochs": [False],
         "max_iter": [1, 2, 4, 7, 10, 20], #, 30, 50],#, 100, 300],
         "learning_rate": np.logspace(-2, 1, 10),
         "strategy": ["GSr"],
         #    "rectify_bad_step": [True, False],
         "clip": np.logspace(-4, 6, 50)
     }
     ),
    (greedy_coordinate_descent,
     {
         "epochs": [False],
         "max_iter": [1, 2, 4, 7, 10, 15, 20], #, 30, 50],#, 100, 300],
         "learning_rate": np.logspace(-2, 1, 10),
         "strategy": ["GSq"],
         #    "rectify_bad_step": [True, False],
         "clip": np.logspace(-4, 6, 50)
     }
     )
]
