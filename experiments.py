import pandas as pd
import numpy as np
import sys
import os
import time
import csv
from tqdm import tqdm

from multiprocessing import Pool

from sklearn.model_selection import ParameterGrid

from pcoptim import LeastSquares, Logistic, SparseLeastSquares, SparseLogistic
from pcoptim import L1Regularizer, L2Regularizer, ElasticNetRegularizer
from pcoptim import coordinate_descent, gradient_descent, greedy_coordinate_descent

# problems to run
from problems import losses_desc

# parameter grids
from params_grid import algo_params_list

# data directory
data_dir = os.path.expanduser("~/research/datasets/")

# output directory
date = time.strftime("%d-%m-%y-%Hh%M") + "/"
out_dir = "./results/" + date

os.makedirs(out_dir, exist_ok=True)


def import_dataset(name, data_dir, norm=None, sparse=False):

    path = data_dir + name + "/" + name

    if not sparse:
        X = np.load(path + "_" + norm + ".data", allow_pickle=True)

    else:
        X = sp.load_npz(path + "_" + norm + ".npz")

    y = np.load(path + ".labels", allow_pickle=True)
    return X, y


def run_algo_with_params(params):
    global algo, loss, w0, priv_params
    global loss_obj, reg_obj, dataset, raw, lbd

    ret = algo(loss, w0, **priv_params, **params, nb_logs=1)

    return  params, ret.obj_[-1]


rng = np.random.default_rng(seed=42)


# for each loss from problem.py
for loss_obj, reg_obj, dataset, raw, lbd, issparse  in losses_desc:

    # load the data and form a global loss variable
    X, y = import_dataset(dataset, data_dir, raw, issparse)

    reg = reg_obj(lbd)
    loss = loss_obj(X, y, reg)

    w0 = np.zeros(loss.p_)

    priv_params = { "epsilon": 1, "delta": 1.0/loss.n_**2 }

    for algo, parameter_grid in algo_params_list:

        with open(out_dir + algo.__name__ + ".csv", "a") as f:

            # initalize csv writer
            keys = list(parameter_grid.keys()) + ["seed"]
            csvwriter = csv.writer(f, lineterminator = '\n')

            # write column names if file is new
            if f.tell() == 0:
                csvwriter.writerow(["dataset", "loss",
                                    "regularizer", "lambda",
                                    "epsilon", "delta",
                                    *keys, "obj"])

            # compute the grid
            pgrid = [{**p, "seed": s}
                     for p in ParameterGrid(parameter_grid)
                     for s in rng.integers(100000, size=2)]

            # run in parallel
            with Pool(30) as p:
                for params, result in p.imap_unordered(run_algo_with_params,
                                                       pgrid):

                    csvwriter.writerow([
                        dataset,
                        loss_obj.__name__,
                        reg_obj.__name__,
                        lbd,
                        priv_params["epsilon"], priv_params["delta"],
                        *[params[k] for k in keys],
                        result
                    ])

                    f.flush()
