import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import pickle

import csv

from tqdm import tqdm

import os

from pcoptim import coordinate_descent, gradient_descent, greedy_coordinate_descent
from pcoptim import LeastSquares, SparseLeastSquares, Logistic, SparseLogistic
from pcoptim import L1Regularizer, L2Regularizer


def import_dataset(name, data_dir, norm=None, sparse=False):

    path = data_dir + name + "/" + name

    if not sparse:
        X = np.load(path + "_" + norm + ".data", allow_pickle=True)

    else:
        X = sp.load_npz(path + "_" + norm + ".npz")

    y = np.load(path + ".labels", allow_pickle=True)
    return X, y


tuning_cd, tuning_sgd, tuning_gcd = pd.DataFrame(), pd.DataFrame(), pd.DataFrame()

# results of tuning
for directory in ["results", "results_others", "results_large_sparse"]:
    tuning_cd = pd.concat((tuning_cd,
                           pd.read_csv("results/" + directory + "/coordinate_descent.csv")),
                          ignore_index=True)
    tuning_sgd = pd.concat((tuning_sgd,
                            pd.read_csv("results/" + directory + "/gradient_descent.csv")),
                           ignore_index=True)
    tuning_gcd = pd.concat((tuning_gcd,
                            pd.read_csv("results/" + directory + "/greedy_coordinate_descent.csv")),
                           ignore_index=True)

print(np.unique(tuning_cd["dataset"]))

# loss objects
loss_dict = {
    "LeastSquares": LeastSquares, "Logistic": Logistic,
    "SparseLeastSquares": SparseLeastSquares, "SparseLogistic": SparseLogistic,
}

reg_dict = {
    "L1Regularizer": L1Regularizer,
    "L2Regularizer": L2Regularizer
}


# data
data_dir = os.path.expanduser("~/research/datasets/")


# sparse datasets
sparse_datasets = ["dorothea", "dexter", "very_large_sparse", "large_sparse", "square_sparse"]

# norm
norm = { "dorothea": "raw",
         "mtp": "norm",
         "lasso": "norm",
         "california": "norm",
         "lognormal_opt_1": "norm",
         "lognormal_opt_2": "norm",
         "dexter": "norm",
         "madelon": "norm",
         "large_sparse": "raw",
         "square_sparse": "raw"
        }


#
def run_algo(algo, df, nruns=5, filter={}):
    global loss_dict, reg_dict, data_dir, sparse_datasets
    global writer, f

    rng = np.random.default_rng(seed=42)


    results = {}

    df = df.copy()
    df = df.drop("seed", axis=1)
    col = list(df.columns[df.columns != "obj"])
    df=df.groupby(col, as_index=False).agg({"obj": np.mean})

    df=df.groupby(["loss", "regularizer", "dataset"])
    for comb, retdf in tqdm(df):

        possible_idx = (retdf["max_iter"] <= 20)
        for key, condition in filter.items():
            possible_idx = possible_idx & condition(retdf[key])

            print(np.unique(np.array(retdf[condition(retdf[key])]["strategy"])))

        idx = np.argmin(retdf[possible_idx]["obj"])

        params = retdf[possible_idx].iloc[idx]
        dataset = params["dataset"]

        writer.writerow([
            params["dataset"], params["loss"], params["regularizer"],
            algo.__name__.replace("_", " "),
            params["max_iter"],
            "%.2e" % params["clip"],
            "%.2e" % params["learning_rate"]
        ])
        f.flush()


        X, y = import_dataset(dataset, data_dir, norm[dataset],
                              sparse=dataset in sparse_datasets)

        reg = reg_dict[params["regularizer"]](params["lambda"])
        loss = loss_dict[params["loss"]](X, y, reg)

        w0 = np.zeros(loss.p_)

        params_dict = params.to_dict()
        for rem in ["dataset", "loss", "regularizer", "lambda", "obj"]:
            del params_dict[rem]

        print(algo.__name__, len(filter) == 0, dataset, params_dict.values())

        ret = [algo(loss, w0, **params_dict, seed=rng.integers(10000))
               for r in range(nruns)]

        results[(params["dataset"], params["loss"], params["regularizer"])] = ret

    return results

with open('parameters/params.csv', 'w', newline='') as f:
    writer = csv.writer(f)
    writer.writerow([
        "dataset", "loss", "regularizer", "iterations", "clipping", "step size"
    ])

    results_dict = {
        "cd": run_algo(coordinate_descent, tuning_cd),
        "sgd": run_algo(gradient_descent, tuning_sgd),
        "gcd": run_algo(greedy_coordinate_descent, tuning_gcd),
        "gcd_gsr": run_algo(greedy_coordinate_descent, tuning_gcd, filter={"strategy": lambda x: x == "GSr"} ),
        "gcd_gss": run_algo(greedy_coordinate_descent, tuning_gcd, filter={"strategy": lambda x: x == "GSs"} ),
        "gcd_gsq": run_algo(greedy_coordinate_descent, tuning_gcd, filter={"strategy": lambda x: x == "GSq"} ),
    }

with open("results/run_results.pkl", "wb") as f:
    pickle.dump(results_dict, f)
