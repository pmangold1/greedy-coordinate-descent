#pragma once
#include <Eigen/Dense>

#include <boost/none_t.hpp>
#include <boost/optional.hpp>
#include <boost/range/adaptor/indexed.hpp>
#include <optional>
#include <string>
#include <random>
#include <stdexcept>
#include <memory>
#include <type_traits>

#include <iostream>


#include "../loss"
#include "../utils/logs.hpp"
#include "../utils/noise_utils.hpp"
#include "../utils/privacy_utils.hpp"
#include "batch.hpp"

using Eigen::MatrixXd;
using Eigen::VectorXd;
using Eigen::VectorXi;

using boost::optional;
using boost::none;
using std::shared_ptr;
using std::string;


// classes for choosing the coordinate to update
template<class Mat>
class GreedyCoordinateStrategy {
public:
  GreedyCoordinateStrategy() {}
  ~GreedyCoordinateStrategy() {}

  virtual int choose(const Loss<Mat>* loss, const VectorXd& w, const vector< optional<double> >& clipping_thresholds, const VectorXd& scale, const optional<VectorXi>& batch_indices, const optional<VectorXd>& residuals, const VectorXd& lr) const = 0;
};

template< class Mat >
class GSqGreedyStrategy : public GreedyCoordinateStrategy<Mat> {
public:
  GSqGreedyStrategy(std::mt19937_64& generator) : generator_(generator) {}
  ~GSqGreedyStrategy() {}

  int choose(const Loss<Mat>* loss, const VectorXd& w, const vector< optional<double> >& clipping_thresholds, const VectorXd& scale, const optional<VectorXi>& batch_indices, const optional<VectorXd>& residuals, const VectorXd& lr) const override {
    int max_j = 0;
    VectorXd noisy_gradient(loss->p());
    VectorXd noisy_update(loss->p());

    for(unsigned int j = 0; j < loss->p(); j++) {

      float grad = 	loss->coord_gradient(w, j, batch_indices,
			     clipping_thresholds[j], residuals);

      noisy_gradient(j) = grad;
      noisy_gradient(j) += zero_mean_noise(scale(j), 1, generator_);

      float gamma = loss->coord_prox(w(j) - lr(j) * noisy_gradient(j),
				     lr(j)) - w(j);

      noisy_update(j) =
	gamma * grad + loss->coord_lipschitz(j) * gamma * gamma / 2
	+ loss->coord_prox(w(j) + gamma, 1.0/loss->coord_lipschitz(j))
	- loss->coord_prox(w(j), 1.0/loss->coord_lipschitz(j));

      if(std::abs(noisy_update(j)) > std::abs(noisy_update(max_j)))
	max_j = j;
    }

    return max_j;

  }

private:
  std::mt19937_64& generator_;
};

template< class Mat >
class GSrGreedyStrategy : public GreedyCoordinateStrategy<Mat> {
public:
  GSrGreedyStrategy(std::mt19937_64& generator) : generator_(generator) {}
  ~GSrGreedyStrategy() {}

  int choose(const Loss<Mat>* loss, const VectorXd& w, const vector< optional<double> >& clipping_thresholds, const VectorXd& scale, const optional<VectorXi>& batch_indices, const optional<VectorXd>& residuals, const VectorXd& lr) const override {

    int max_j = 0;
    VectorXd noisy_gradient(loss->p());
    VectorXd noisy_update(loss->p());

    for(unsigned int j = 0; j < loss->p(); j++) {

      noisy_gradient(j) =
	loss->coord_gradient(w, j, batch_indices,
			     clipping_thresholds[j], residuals);
      noisy_gradient(j) += zero_mean_noise(scale(j), 1, generator_);

      noisy_update(j) =
	std::sqrt(1.0/loss->coord_lipschitz(j))
	* (loss->coord_prox(w(j) - lr(j) * noisy_gradient(j),
			    lr(j)) - w(j));

      if(std::abs(noisy_update(j)) > std::abs(noisy_update(max_j)))
	max_j = j;
    }

    return max_j;

  }

private:
  std::mt19937_64& generator_;
};

template< class Mat >
class GSsGreedyStrategy : public GreedyCoordinateStrategy<Mat> {
public:
  GSsGreedyStrategy(std::mt19937_64& generator) : generator_(generator) {}
  ~GSsGreedyStrategy() {}


  int choose(const Loss<Mat>* loss, const VectorXd& w, const vector< optional<double> >& clipping_thresholds, const VectorXd& scale, const optional<VectorXi>& batch_indices, const optional<VectorXd>& residuals, const VectorXd& lr) const override {
    int max_j = 0;
    VectorXd noisy_gradient(loss->p());
    VectorXd noisy_update(loss->p());

    for(unsigned int j = 0; j < loss->p(); j++) {

      noisy_gradient(j) =
	loss->coord_gradient(w, j, batch_indices,
			     clipping_thresholds[j], residuals);
      noisy_gradient(j) += zero_mean_noise(scale(j), 1, generator_);
    }

    for(unsigned int j = 0; j < loss->p(); j++) {
      noisy_update(j) =
	std::sqrt(1.0/loss->coord_lipschitz(j))
	* loss->coord_min_abs_subgradient(w, noisy_gradient, j);

      if(std::abs(noisy_update(j)) > std::abs(noisy_update(max_j)))
	max_j = j;
    }

    return max_j;

  }

private:
  std::mt19937_64& generator_;
};

template< class Mat >
GreedyCoordinateStrategy<Mat>* create_greedy_coordinate_strategy(string strategy, std::mt19937_64& generator) {
  if(strategy == "GSs") return new GSsGreedyStrategy<Mat>(generator);
  if(strategy == "GSq") return new GSqGreedyStrategy<Mat>(generator);
  else return new GSrGreedyStrategy<Mat>(generator);
}

/// Run coordinate descent on a given loss.
template<class Mat>
Log greedy_coordinate_descent(const Loss<Mat>* loss,
		       const VectorXd& w0,
		       const double& max_iter=100,
		       const double& learning_rate=1.0,
		       const int& nb_logs=10,
		       const optional<double>& clip=none,
		       const double& alpha_clip=1.0,
		       const string& strategy="GSr",
		       const bool& rectify_bad_step=true,
		       //		       const string clip_strategy="lipschitz",
		       const optional<int>& batch_size=none,
		       const string batch_strategy="shuffle",
		       const optional<VectorXd>& coord_lip=none,
		       const double& epsilon=0,
		       double delta=-1,
		       const int& q=2,
		       int seed=-1,
		       const bool& epochs=true) {

  // compute appropriate number of iterations
  double nb_iter_double = max_iter;
  if(epochs) {
    if(batch_size != none)
      nb_iter_double *= loss->n() / (*batch_size);
    nb_iter_double *= loss->p();
  }
  int nb_iter = nb_iter_double;
  nb_iter = std::max(nb_iter, 1);

  // initialization
  Log logs(nb_iter, nb_logs);
  VectorXd w = w0;
  VectorXd lr(loss->p());
  for(unsigned int j = 0; j < loss->p(); j++) {
    if(coord_lip == none)
      lr(j) = learning_rate / loss->coord_lipschitz(j);
    else
      lr(j) = learning_rate / (*coord_lip)(j);
  }

  // x step length
  double xstep = (batch_size == none)
    ? 1.0
    : (double)*batch_size / loss->n();
  if(epochs) xstep /= loss->p();

  // residuals
  optional<VectorXd> residuals = none;
  if(batch_size == none) residuals = loss->get_residuals(w);

  // random number generator
  std::mt19937_64 generator(seed);

  // random batch generator
  optional<VectorXi> batch_indices = boost::make_optional(false, VectorXi());
  BatchGenerator* rbg = create_batch_generator(loss->n(), batch_strategy, generator);
  double subsampling_ratio = (batch_size == none) ? -1 : (double)(*batch_size) / loss->n();

  // strategy for greedy update
  GreedyCoordinateStrategy<Mat>* gss = create_greedy_coordinate_strategy<Mat>(strategy, generator);


  // clipping thresholds
  VectorXd lip = (coord_lip == none) ?
    loss->get_all_lipschitz_coord()
    : *coord_lip;

  vector< optional<double> > clipping_thresholds(loss->p(), none);
  if(clip != none) {
    // compute lipschitz constants and raise them to power alpha_clip
    // if the constants are provided as argument, use them
    VectorXd lipalpha(loss->p());

    for(unsigned int j = 0; j < loss->p(); j++) {
      lipalpha(j) = std::pow(lip(j), alpha_clip);
    }

    // compute thresholds
    // if alpha_clip = 0, clip uniformly to 1/sqrt(p)
    // else adapt to coord gradient lipschitz constants
    double lipsum = lipalpha.sum();
    for(unsigned int j = 0; j < loss->p(); j++) {
      clipping_thresholds[j] = (*clip) * std::sqrt(lipalpha(j) / lipsum);
    }
  }

  // privacy needs
  VectorXd scale = VectorXd::Zero(loss->p());
  if(epsilon > 0) {
    // if no clipping provided, return immediately
    if(clip == none) {
      throw std::invalid_argument("greedy_coordinate_descent: if epsilon > 0, clipping must be provided.");
    }

    // default delta if not provided
    if(delta <= 0) delta = 1.0 / pow(loss->n(), 2);

    // compute scale if algorithm is noisy
    double priv_constant = best_privacy_constant_laplace(2*nb_iter, epsilon, delta, subsampling_ratio);
    //    std::cout << "CD - priv constant : " << priv_constant << std::endl;

    for(unsigned int j = 0; j < loss->p(); j++) {
      scale(j) = priv_constant * 2 * (*(clipping_thresholds[j])) / (loss->n() * subsampling_ratio);
	//	priv_constant * std::sqrt(nb_iter * std::log(1.0/delta))
	//	* (*(clipping_thresholds[j])) / (loss->n() * epsilon);
    }
  }

  // choice of updated parameter and full noisy gradient
  int j = 0;

  // log initial values
  logs.init_timer();
  logs.write(0, loss->evaluate(w), w);


  for(unsigned int t = 1; t < nb_iter + 1; t++) {

    // get new batch
    if(batch_size != none) batch_indices = rbg->get_batch(*batch_size);

    // compute next coordinate and related update
    j = gss->choose(loss, w, clipping_thresholds, scale, batch_indices, residuals, lr);

    // remember old value of wj
    double wj_old = w(j);

    // update
    double grad_j = loss->coord_gradient(w, j, batch_indices,
					 clipping_thresholds[j], residuals);
    double noise = zero_mean_noise(scale(j), 1, generator);
    w(j) = loss->coord_prox(w(j) - lr(j) * (grad_j + noise), lr(j));

    // if bad step
    if(rectify_bad_step)
      if(wj_old * w(j) < 0)
	w(j) = 0;

    // update residual
    if(batch_size == none) residuals = loss->coord_update_residuals(residuals, w(j) - wj_old, j);

    // log current values (if needed at this step)
    if(logs.should_log(t)) {
      logs.write((double)t * xstep, loss->evaluate(w, residuals), w);
    }
  }

  logs.final_coef = w;
  logs.final_obj = loss->evaluate(w);

  delete rbg;
  delete gss;

  return logs;
}
