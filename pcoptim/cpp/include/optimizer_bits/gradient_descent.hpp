#pragma once
#include <Eigen/Dense>

#include <boost/none_t.hpp>
#include <boost/optional.hpp>
#include <boost/range/adaptor/indexed.hpp>
#include <optional>
#include <string>
#include <random>
#include <memory>

#include <iostream>

#include "../loss"
#include "../utils/logs.hpp"
#include "../utils/noise_utils.hpp"
#include "batch.hpp"

using Eigen::MatrixXd;
using Eigen::VectorXd;
using Eigen::VectorXi;

using boost::optional;
using boost::none;
using std::shared_ptr;
using std::string;

/// Run gradient descent on a given loss.
template<class Mat>
Log gradient_descent(const Loss<Mat>* loss,
		     const VectorXd& w0,
		     const double& max_iter=100,
		     const double& learning_rate=1.0,
		     const int& nb_logs=10,
		     const optional<double>& clip=none,
		     const optional<int>& batch_size=none,
		     const string batch_strategy="shuffle",
		     const optional<double>& lip=none,
		     const double& epsilon=0,
		     double delta=-1,
		     const int& q=2,
		     int seed=-1,
		     const bool& epochs=true) {

  // compute appropriate number of iterations
  double nb_iter_double = max_iter;
  if(epochs) {
    if(batch_size != none)
      nb_iter_double *= loss->n() / (*batch_size);
  }
  int nb_iter = nb_iter_double;
  nb_iter = std::max(nb_iter, 1);

  // initialization
  Log logs(nb_iter, nb_logs);
  VectorXd w = w0;

  double lipschitz = (lip == none) ?
    loss->lipschitz()
    : *lip;
  double lr = learning_rate / lipschitz;

  // x step length
  double xstep = (batch_size == none)
    ? 1.0
    : (double)*batch_size / loss->n();

  // random number generator
  std::mt19937_64 generator(seed);

  // batch
  optional<VectorXi> batch_indices = boost::make_optional(false, VectorXi());

  // random batch generator
  BatchGenerator* rbg = create_batch_generator(loss->n(), batch_strategy, generator);
  double subsampling_ratio = (batch_size == none) ? -1 : (double)(*batch_size) / loss->n();

  // privacy needs
  double scale = 0;
  if(epsilon > 0) {
    // if no clipping provided, return immediately
    if(clip == none) {
      throw std::invalid_argument("gradient_descent: if epsilon > 0, clipping must be provided.");
    }

    // default delta if not provided
    if(delta <= 0) delta = 1.0 / pow(loss->n(), 2);

    // compute scale if algorithm is noisy

    double priv_constant = best_privacy_constant_rdp(nb_iter, epsilon, delta, subsampling_ratio);

    scale = priv_constant * 2 * (*clip) / (loss->n() * subsampling_ratio);

    //      std::sqrt(8 * nb_iter * std::log(1.0/delta)) * (*clip) / (loss->n() * epsilon);
  }

  // log initial values
  logs.init_timer();
  logs.write(0, loss->evaluate(w), w);


  for(unsigned int t = 1; t < nb_iter + 1; t++) {

    // get new batch
    if(batch_size != none) batch_indices = rbg->get_batch(*batch_size);

    // compute gradient
    VectorXd gradient = loss->gradient(w, batch_indices, clip);

    // compute noise
    VectorXd noise = zero_mean_noise_vector(scale, loss->p(), q, generator);

    // update
    w = loss->prox(w - lr * (gradient + noise), lr);

    // log current values (if needed at this step)
    if(logs.should_log(t)) {
      logs.write((double)t * xstep, loss->evaluate(w), w);
    }
  }

  logs.final_coef = w;
  logs.final_obj = loss->evaluate(w);

  delete rbg;
  return logs;
}
