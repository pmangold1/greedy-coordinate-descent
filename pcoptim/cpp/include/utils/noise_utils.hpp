#pragma once
#include <random>
#include <boost/random/laplace_distribution.hpp>
#include <Eigen/Dense>

using Eigen::VectorXd;

/// Sample one point from a centred normal distribution.
/// @param RNG Type of the random number generator.
/// @param scale Standard deviation of the noise.
/// @param generator A random number generator.
template< class RNG >
double sample_from_normal(const double& scale, RNG& generator) {
  using param_type =
    typename std::normal_distribution<double>::param_type;

  // static generator and distribution to avoid recreating one object each time
  static std::normal_distribution<double> norm_dist;

  return norm_dist(generator, param_type{0, scale});
}


/// Sample one point from a centred laplace distribution.
/// @param RNG Type of the random number generator.
/// @param scale Standard deviation of the noise.
/// @param generator A random number generator.
template< class RNG >
double sample_from_laplace(const double& scale, RNG& generator) {
  using param_type =
    typename boost::random::laplace_distribution<double>::param_type;

  // static std::default_random_engine generator;
  static boost::random::laplace_distribution<double> lap_dist;

  return lap_dist(generator, param_type{0, scale});
}




/// Sample a vector from a centred normal distribution.
/// @param RNG Type of the random number generator.
/// @param scale Standard deviation of the noise.
/// @param size Number of points to sample.
/// @param generator A random number generator.
template< class RNG >
VectorXd zero_mean_normal_vector(const double& scale, const unsigned int& size,
				 RNG &generator) {

  VectorXd noise(size);

  for(unsigned int i = 0; i < size; i++) {
    // generation of noise with given distribution
    noise(i) = sample_from_normal(scale, generator);
  }

  return noise;
}


/// Sample a vector from a centred laplace distribution.
/// @param RNG Type of the random number generator.
/// @param scale Standard deviation of the noise.
/// @param size Number of points to sample.
/// @param generator A random number generator.
template< class RNG >
VectorXd zero_mean_laplace_vector(const double& scale, const unsigned int& size,
				  RNG& generator) {

  VectorXd noise(size);

  for(unsigned int i = 0; i < size; i++) {
    noise(i) = sample_from_laplace(scale, generator);
  }

  return noise;
}

/// Return a sampled vector from distribution corresponding to a privacy mechanism.
/// @param RNG Type of the random number generator.
/// @param scale Standard deviation of the noise.
/// @param size Number of points to sample.
/// @param q Type of the mechanism (1 = Laplace, 2 = Normal).
/// @param generator A random number generator.template< class RNG >
template< class RNG >
VectorXd zero_mean_noise_vector(const double& scale, const unsigned int& size,
				const unsigned int& q, RNG& generator) {
  if(q == 2) {
    return zero_mean_normal_vector(scale, size, generator);
  }

  else {
    return zero_mean_laplace_vector(scale, size, generator);
  }
}

/// Return a sampled point from distribution corresponding to a privacy mechanism.
/// @param RNG Type of the random number generator.
/// @param scale Standard deviation of the noise.
/// @param q Type of the mechanism (1 = Laplace, 2 = Normal).
/// @param generator A random number generator.
template< class RNG >
double zero_mean_noise(const double& scale,
		       const unsigned int& q, RNG& generator) {

  if(q == 2) {
    return sample_from_normal(scale, generator);
  }

  else {
    return sample_from_laplace(scale, generator);
  }
}

/// Generate a random integer in a given interval.
/// @param RNG Type of the random number generator.
/// @param b Beginning of the interval (included).
/// @param e End the interval (not included).
/// @param generator A random number generator.
template< class RNG >
unsigned int random_int(const unsigned int& b, const unsigned int& e, RNG& generator) {
  using param_type =
    typename std::uniform_int_distribution<unsigned int>::param_type;

  // static generator and distribution to avoid recreating one object each time
  static std::uniform_int_distribution<unsigned int> unif_dist;

  return unif_dist(generator, param_type{b, e - 1});
}

/// Generate a vector of random integers in a given interval.
/// @param RNG Type of the random number generator.
/// @param size Number of points to sample.
/// @param b Beginning of the interval (included).
/// @param e End the interval (not included).
/// @param generator A random number generator.
template< class RNG >
VectorXi random_int_vector(const unsigned int& size,
			   const unsigned int& b, const unsigned int& e, RNG& generator) {

  VectorXi ret(size);

  for(unsigned int i = 0; i < size; i++) {
    ret(i) = random_int(b, e, generator);
  }

  return ret;
}
