#ifndef LOGISTIC_HEADER_
#define LOGISTIC_HEADER_

#include "loss.hpp"
#include "loss-utils.hpp"

template<class Mat>
class Logistic : public Loss<Mat> {
public:

  Logistic(const Mat& X, const VectorXd& y, const Regularizer* reg)
    : Loss<Mat>(X, discretize(y), reg) {

    this->compute_bounds_and_constants();

  }

  Logistic(const Mat& X, const VectorXd& y)
    : Loss<Mat>(X, discretize(y)) {

    this->compute_bounds_and_constants();

  }

  ~Logistic() {

  }

  // residual computation
  double compute_pointwise_residual_(const VectorXd& w,
				     const int& i) const override {
    return this->y_(i) * this->X_.row(i).dot(w);
  }


  VectorXd compute_global_residual_(const VectorXd& w) const override {
    return this->y_.array() * (this->X_ * w).array();
  }

  VectorXd compute_batch_residual_(const VectorXd& w, const VectorXi& batch) const override {
    return this->y_(batch).array() * (this->X_(batch, Eigen::all) * w).array();
  }

  double coord_update_pointwise_residual_(const double& residuali,
					  const double& w_update,
					  const int& i, const int& j) const override {
    return residuali + this->y_(i) * this->X_(i, j) * w_update;
  }



  // sensitivity
  double sensitivity(const VectorXd&) const override {
    return 0;
  }

  double coord_sensitivity(const VectorXd&, const int&) const override {
    return 0;
  }

private:

  // function evaluation
  double evaluate_(const VectorXd& w, const optional<VectorXd>& residuals = none) const override {
    VectorXd residuals_ = (residuals == none) ?
      this->get_residuals(w) : *residuals;
    double obj = 0;

    for(unsigned int i = 0; i < this->n_; i++) {
      obj += std::log(1 + std::exp(- residuals_(i)));
    }

    return obj / this->n_;
  }

  // gradient
  double pointwise_coord_gradient_(const VectorXd& w, const int& i, const int& j,
				   const optional<double>& residual = none) const override {

    // if residual is provided, use itt line, otherwise compute it
    double residuali = (residual == none) ?
      this->compute_pointwise_residual_(w, i) : *residual;

    // compute gradient from residual and return it
    return - this->y_(i) / (1.0 + std::exp(residuali)) * this->X_(i, j);
  }

  // lipschitz
  double lipschitz_() const override {
    return 0.25 / this->n_ * this->XTX_bound_;
  }

  double coord_lipschitz_(const int& j) const override {
    return 0.25 / this->n_ * std::pow(this->X2_feat_bound_(j), 2);
  }
};



#endif
