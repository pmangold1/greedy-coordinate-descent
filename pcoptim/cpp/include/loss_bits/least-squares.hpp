#ifndef LEASTSQUARES_HEADER_
#define LEASTSQUARES_HEADER_

#include "loss.hpp"
#include "loss-utils.hpp"

template<class Mat>
class LeastSquares : public Loss<Mat> {
public:

  LeastSquares(const Mat& X, const VectorXd& y, const Regularizer* reg)
    : Loss<Mat>(X, y, reg) {

    this->compute_bounds_and_constants();

  }

  LeastSquares(const Mat& X, const VectorXd& y)
    : Loss<Mat>(X, y) {

    this->compute_bounds_and_constants();

  }

  ~LeastSquares() {

  }

  // sensitivity
  double sensitivity(const VectorXd&) const override {
    return 0;
  }

  double coord_sensitivity(const VectorXd&, const int&) const override {
    return 0;
  }



private:
  // function evaluation
  double evaluate_(const VectorXd& w, const optional<VectorXd>& residuals = none) const override {
    VectorXd residuals_ = this->get_residuals(w, none, residuals);

    return 0.5 / this->n_ * residuals_.squaredNorm();
  }

  // gradient
  double pointwise_coord_gradient_(const VectorXd& w, const int& i, const int& j,
				   const optional<double>& residual = none) const override {

    // if residual is provided, use itt line, otherwise compute it
    double residuali = (residual == none) ?
      this->compute_pointwise_residual_(w, i) : *residual;

    // compute gradient from residual and return it
    return this->X_(i, j) * residuali;
  }

  // lipschitz
  double lipschitz_() const override {
    return 1.0 / this->n_ * this->XTX_bound_;
  }

  double coord_lipschitz_(const int& j) const override {
    return 1.0 / this->n_ * std::pow(this->X2_feat_bound_(j), 2);
  }


  // residuals
  double compute_pointwise_residual_(const VectorXd& w,
				     const int& i) const override {
   return this->X_.row(i) * w - this->y_(i);
  }

  VectorXd compute_global_residual_(const VectorXd& w) const override {
    return this->X_ * w - this->y_;
  }

  VectorXd compute_batch_residual_(const VectorXd& w, const VectorXi& batch) const override {
    return this->X_(batch, Eigen::all) * w - this->y_(batch);
  }

  double coord_update_pointwise_residual_(const double& residuali,
					  const double& w_update,
					  const int& i, const int& j) const override {
    return residuali + this->X_(i, j) * w_update;
  }


};

#endif
