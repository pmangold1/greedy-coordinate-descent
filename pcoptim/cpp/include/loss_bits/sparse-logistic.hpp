#ifndef SPARSE_LOGISTIC_HEADER_
#define SPARSE_LOGISTIC_HEADER_

#include "loss.hpp"
#include "loss-utils.hpp"

class SparseLogistic : public Loss<SpMat> {
public:

  SparseLogistic(const SpMat& X, const VectorXd& y, const Regularizer* reg)
    : Loss<SpMat>(X, discretize(y), reg) {

    XT_ = X;
    this->compute_bounds_and_constants();

  }

  SparseLogistic(const SpMat& X, const VectorXd& y)
    : Loss<SpMat>(X, discretize(y)) {

    XT_ = X;
    this->compute_bounds_and_constants();

  }

  ~SparseLogistic() {

  }

  double compute_pointwise_residual_(const VectorXd& w,
				     const int& i) const override {
    return this->y_(i) * this->XT_.row(i).dot(w);
  }


  VectorXd compute_batch_residual_(const VectorXd& w, const VectorXi& batch) const override {
    VectorXd ret = VectorXd(batch.size());

    for(unsigned int i = 0; i < batch.size(); i++) {
      int idx = batch(i);

      ret[i] = this->y_(idx) * this->XT_.row(idx).dot(w);
    }
    return ret;
  }


  double coord_update_pointwise_residual_(const double& residuali,
					  const double& w_update,
					  const int& i, const int& j) const override {
    return -1;
  }

  optional<VectorXd> coord_update_residuals(optional<VectorXd> residuals,
					    const double& w_update=0, const int& j=0) const {

    if(residuals == none)
      return none;
    else {
      *residuals +=  w_update * this->y_.cwiseProduct(this->X_.col(j));
    }
    return residuals;
  }


  // sensitivity
  double sensitivity(const VectorXd&) const override {
    return 0;
  }

  double coord_sensitivity(const VectorXd&, const int&) const override {
    return 0;
  }


  VectorXd gradient(const VectorXd& w,
		    const optional<VectorXi>& batch = none,
		    const optional<double>& clip_threshold = none,
		    const optional<VectorXd>& residuals = none) const {

    VectorXd grad = VectorXd::Zero(w.size());
    VectorXd residuals_ = get_residuals(w, batch, residuals);

    // if no batch provided, compute complete gradient
    if(batch == none) {
      for(unsigned int i = 0; i < n_; i++) {
	double res_ = get_pointwise_residual(w, i, residuals);

	//	VectorXd g = pointwise_gradient(w, i, clip_threshold, res_);
	grad += pointwise_gradient(w, i, clip_threshold, res_);
      }
      grad /= n_;
    }
    // otherwise use only selected indices
    else {
      for(unsigned int i = 0; i < (*batch).size(); i++) {
	double res_ = get_pointwise_residual(w, (*batch)(i), residuals);
	grad += pointwise_gradient(w, (*batch)(i), clip_threshold, res_);
      }
      grad /= (*batch).size();
    }

    // return gradient + gradient coming from the regularizer
    return grad + regularizer_->gradient(w);
  }


private:

  // function evaluation
  double evaluate_(const VectorXd& w, const optional<VectorXd>& residuals = none) const override {
    VectorXd residuals_ = (residuals == none) ?
      this->get_residuals(w) : *residuals;
    double obj = 0;

    for(unsigned int i = 0; i < this->n_; i++) {
      obj += std::log(1 + std::exp(- residuals_(i)));
    }

    return obj / this->n_;
  }


  double pointwise_coord_gradient_(const VectorXd& w, const int& i, const int& j,
				   const optional<double>& residual = none) const override {
    return -1;
  }

  double coord_gradient(const VectorXd& w, const int& j,
			const optional<VectorXi>& batch = none,
			const optional<double>& clip_threshold = none,
			const optional<VectorXd>& residuals = none) const {

    double gradj = 0;
    VectorXd residuals_ = get_residuals(w, batch, residuals);


    // if no batch provided, compute complete coordinate gradient
    if(batch == none) {
      for (SpMat::InnerIterator it(this->X_, j); it; ++it) {
	double res_ = get_pointwise_residual(w, it.row(), residuals_);
	double grad_ = - this->y_(it.row()) / (1.0 + std::exp(res_)) * it.value(); //pointwise_coord_gradient(w, i, j, clip_threshold, res_);

	gradj += clip(grad_, clip_threshold);
      }
      gradj /= n_;
    }

    else {
      // not implemented error
    }

    return gradj + regularizer_->coord_gradient(w, j);
  }

  VectorXd pointwise_gradient_(const VectorXd& w,
			       const unsigned int& i,
			       const optional<double>& residual = none) const override {

    double residuali = (residual == none) ?
      this->compute_pointwise_residual_(w, i) : *residual;

    VectorXd gradi(p_);
    gradi = - this->y_(i) / (1.0 + std::exp(residuali)) * this->X_.row(i);

    // return gradient
    return gradi;
  }


  // lipschitz
  double lipschitz_() const override {
    return 0.25 / this->n_ * this->XTX_bound_;
  }

  double coord_lipschitz_(const int& j) const override {
    return 0.25 / this->n_ * std::pow(this->X2_feat_bound_(j), 2);
  }


};


#endif
