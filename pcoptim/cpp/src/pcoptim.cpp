#include <pybind11/pybind11.h>
#include "../h/loss.hpp"


namespace py = pybind11;

PYBIND11_MODULE(pcoptim,m)
{
  m.doc() = "pcoptim";

  //  m.def("trans", &trans);

  py::class_<LeastSquares>(m, "LeastSquares")
    .def(py::init<LeastSquares>());
    //  .def("mul", &CustomVectorXd::mul,pybind11::arg("factor")=1.)
    //  .def("__repr__",
    //    [](const CustomVectorXd &a) {
    //      return "<example.CustomVectorXd>";
}
