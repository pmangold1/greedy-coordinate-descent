#include <pybind11/numpy.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
#include <pybind11/eigen.h>
#include <string>

#include <Eigen/Dense>
#include <Eigen/Sparse>

#include "../cpp/include/loss"
#include "../cpp/include/optimizer"
//#include "../cpp/include/utils/logs.hpp"

namespace py = pybind11;
using namespace pybind11::literals;


typedef Eigen::SparseMatrix<double> SpMat;
using Eigen::MatrixXd;


using std::string;

// `boost::optional` as an example -- can be any `std::optional`-like container
// see https://pybind11.readthedocs.io/en/stable/advanced/cast/stl.html?highlight=boost%20optional#c-17-library-containers
namespace pybind11 { namespace detail {
    template <typename T>
    struct type_caster<boost::optional<T>> : optional_caster<boost::optional<T>> {};
}}

template<class Mat>
void init_loss(py::module& m, const char* loss_name)  {

  py::class_< Loss<Mat> >(m,  loss_name)
    .def_property_readonly("X_", &Loss<Mat>::X)
    .def_property_readonly("y_", &Loss<Mat>::y)
    .def_property_readonly("n_", &Loss<Mat>::n)
    .def_property_readonly("p_", &Loss<Mat>::p)

    .def("gradient", &Loss<Mat>::gradient,
	 py::arg("w"), py::arg("batch") = py::none(),
	 py::arg("clip") = py::none(), py::arg("residuals") = py::none())

    .def("clipped_standardized_gradient", &Loss<Mat>::clipped_standardized_gradient,
	 py::arg("w"), py::arg("m"), py::arg("b"),
	 py::arg("batch") = py::none(), py::arg("residuals") = py::none())

    .def("coord_gradient", &Loss<Mat>::coord_gradient,
	 py::arg("w"), py::arg("j"), py::arg("batch") = py::none(),
	 py::arg("clip") = py::none(), py::arg("residuals") = py::none())

    .def("get_residuals", &Loss<Mat>::get_residuals,
	 py::arg("w"), py::arg("batch") = py::none(),
	 py::arg("residuals") = py::none())

    .def("get_pointwise_residual", &Loss<Mat>::get_pointwise_residual,
	 py::arg("w"), py::arg("i"),
	 py::arg("residuals") = py::none())

    .def("coord_update_residuals", &Loss<Mat>::coord_update_residuals,
	 py::arg("residuals") = py::none(),
	 py::arg("w_update") = 0, py::arg("j") = 0)

    .def("get_all_lipschitz_coord", &Loss<Mat>::get_all_lipschitz_coord)

    .def("prox", &Loss<Mat>::prox,
	 py::arg("w"), py::arg("lr"))

    .def("coord_prox", py::overload_cast<const VectorXd&, const int&, const double&>(&Loss<Mat>::coord_prox, py::const_),
	 py::arg("w"), py::arg("j"), py::arg("lr"))

    .def("coord_prox", py::overload_cast<const double&, const double&>(&Loss<Mat>::coord_prox, py::const_),
	 py::arg("w"), py::arg("lr"))

    .def("evaluate", &Loss<Mat>::evaluate,
	 py::arg("w"), py::arg("residuals") = py::none())

    .def("lipschitz", &Loss<Mat>::lipschitz)

    .def("coord_lipschitz", &Loss<Mat>::coord_lipschitz)

    .def("compute_bounds_and_constants", &Loss<Mat>::compute_bounds_and_constants);
}


void init_losses_and_reg(py::module& m) {
  py::class_< LeastSquares<MatrixXd>, Loss<MatrixXd> >(m, "LeastSquares")
    .def(py::init< const MatrixXd&, const VectorXd& >())
    .def(py::init< const MatrixXd&, const VectorXd&, const Regularizer* >());

  py::class_<SparseLeastSquares, Loss<SpMat> >(m, "SparseLeastSquares")
    .def(py::init< const SpMat&, const VectorXd& >())
    .def(py::init< const SpMat&, const VectorXd&, const Regularizer* >());


  py::class_<Logistic<MatrixXd>, Loss<MatrixXd> >(m, "Logistic")
    .def(py::init< const MatrixXd&, const VectorXd& >())
    .def(py::init< const MatrixXd&, const VectorXd&, const Regularizer* >());

  py::class_<SparseLogistic, Loss<SpMat> >(m, "SparseLogistic")
    .def(py::init< const SpMat&, const VectorXd& >())
    .def(py::init< const SpMat&, const VectorXd&, const Regularizer* >());


  py::class_<Regularizer>(m, "Regularizer")
    .def("evaluate", &Regularizer::evaluate,
	 py::arg("w"))
    .def("gradient", &Regularizer::gradient,
	 py::arg("w"))
    .def("coord_gradient", &Regularizer::coord_gradient,
	 py::arg("w"), py::arg("j"))
    .def("prox", &Regularizer::prox,
	 py::arg("w"), py::arg("lr"))
    .def("coord_prox", py::overload_cast<const VectorXd&, const int&, const double&>(&Regularizer::coord_prox, py::const_),
	 py::arg("w"), py::arg("j"), py::arg("lr"))
    .def("coord_prox", py::overload_cast<const double&, const double&>(&Regularizer::coord_prox, py::const_),
	 py::arg("w"), py::arg("lr"))
    .def("min_abs_subgradient", &Regularizer::min_abs_subgradient,
	 py::arg("w"), py::arg("g"))
    .def("coord_min_abs_subgradient", &Regularizer::coord_min_abs_subgradient,
	 py::arg("w"), py::arg("g"), py::arg("j"));

  py::class_<NullRegularizer, Regularizer>(m, "NullRegularizer")
    .def(py::init< const double &>());

  py::class_<L1Regularizer, Regularizer>(m, "L1Regularizer")
    .def(py::init< const double &>());

  py::class_<ElasticNetRegularizer, Regularizer>(m, "ElasticNetRegularizer")
    .def(py::init< const double &, const double & >());

  py::class_<L2Regularizer, Regularizer>(m, "L2Regularizer")
    .def(py::init< const double &>());

  py::class_<BoxRegularizer, Regularizer>(m, "BoxRegularizer")
    .def(py::init< const double &>());

}


template<class Mat>
void init_algos(py::module& m) {
  m.def("coordinate_descent", &coordinate_descent<Mat>,
	py::arg("loss"),
	py::arg("w0"),
	py::arg("max_iter") = 100,
	py::arg("learning_rate") = 1.0,
	py::arg("nb_logs") = 10,
	py::arg("strategy") = "cycle",
	py::arg("alpha_sample") = 0.0,
	py::arg("clip") = py::none(),
	py::arg("alpha_clip") = 1.0,
	py::arg("batch_size") = py::none(),
	py::arg("batch_strategy") = "shuffle",
	py::arg("coord_lip") = py::none(),
	py::arg("epsilon") = 0,
	py::arg("delta") = -1,
	py::arg("q") = 2,
	py::arg("seed") = -1,
	py::arg("epochs") = true
	);

    m.def("greedy_coordinate_descent", &greedy_coordinate_descent<Mat>,
	py::arg("loss"),
	py::arg("w0"),
	py::arg("max_iter") = 100,
	py::arg("learning_rate") = 1.0,
	py::arg("nb_logs") = 10,
	py::arg("clip") = py::none(),
	py::arg("alpha_clip") = 1.0,
	py::arg("strategy") = "GS-r",
	py::arg("rectify_bad_step") = true,
	py::arg("batch_size") = py::none(),
	py::arg("batch_strategy") = "shuffle",
	py::arg("coord_lip") = py::none(),
	py::arg("epsilon") = 0,
	py::arg("delta") = -1,
	py::arg("q") = 2,
	py::arg("seed") = -1,
	py::arg("epochs") = true
	);


  m.def("gradient_descent", &gradient_descent<Mat>,
	py::arg("loss"),
	py::arg("w0"),
	py::arg("max_iter") = 100,
	py::arg("learning_rate") = 1.0,
	py::arg("nb_logs") = 10,
	py::arg("clip") = py::none(),
	py::arg("batch_size") = py::none(),
	py::arg("batch_strategy") = "shuffle",
	py::arg("lip") = py::none(),
	py::arg("epsilon") = 0,
	py::arg("delta") = -1,
	py::arg("q") = 2,
	py::arg("seed") = -1,
	py::arg("epochs") = true
	);
}


void init_rest(py::module& m) {
  m.def("epsilon_rdp", &epsilon_rdp,
	py::arg("k"),
	py::arg("sigma"),
	py::arg("delta")
	)
;
  m.def("epsilon_sub_rdp", &epsilon_sub_rdp,
	py::arg("k"),
	py::arg("sigma"),
	py::arg("delta"),
	py::arg("subsampling_ratio")
	);

  m.def("best_privacy_constant_rdp", &best_privacy_constant_rdp,
	py::arg("k"),
	py::arg("epsilon"),
	py::arg("delta"),
	py::arg("subsampling_ratio")
	);


  m.def("epsilon_sub_rdp_alpha", &epsilon_sub_rdp_alpha,
	py::arg("k"),
	py::arg("sigma"),
	py::arg("delta"),
	py::arg("q"),
	py::arg("alpha")
	);


  py::class_<Log>(m, "Log")

    .def_readwrite("final_coef", &Log::final_coef)
    .def_readwrite("final_obj", &Log::final_obj)
    .def_property_readonly("x_", &Log::x)
    .def_property_readonly("obj_", &Log::obj)
    .def_property_readonly("elapsed_time_", &Log::elapsed_time)
    .def_property_readonly("params_", &Log::params)

    .def(py::init())

    .def("write_to_file", py::overload_cast< const string& >(&Log::write_to_file, py::const_),
	   py::arg("fpath"))

    .def("to_dict", // TODO: add return of coefficients
	 [](const Log &log) {
	   return py::dict(
			   "x"_a = py::array(log.x().size(), log.x().data()),
			   "obj"_a = py::array(log.obj().size(), log.obj().data()),
			   "time"_a = py::array(log.elapsed_time().size(), log.elapsed_time().data())
			   );
	 }
	 )

    .def(py::pickle(
        [](const Log &p) { // __getstate__
	  /* Return a tuple that fully encodes the state of the object */
	  return py::make_tuple(p.elapsed_time(),
				p.x(),
				p.obj(),
				p.params());
        },
        [](py::tuple t) { // __setstate__

	  // for retrocompatibility
	  if (t.size() == 3) {
	    Log p(t[0].cast< std::vector<double> >(),
		  t[1].cast< std::vector<double> >(),
		  t[2].cast< std::vector<double> >()
		  );
	    return p;
	  }
	  else {
	    Log p(t[0].cast< std::vector<double> >(),
		  t[1].cast< std::vector<double> >(),
		  t[2].cast< std::vector<double> >(),
		  t[3].cast< std::vector< std::vector<double> > >()
		  );
	    return p;
	  }
        }));
}

namespace loss {

PYBIND11_MODULE(pcoptim, m) {
    // Optional docstring
    m.doc() = "Private Optimization Library";

    init_loss<MatrixXd>(m, "Loss");
    init_loss<SpMat>(m, "SparseLoss");

    init_losses_and_reg(m);

    init_algos<MatrixXd>(m);
    init_algos<SpMat>(m);

    init_rest(m);
}

}
