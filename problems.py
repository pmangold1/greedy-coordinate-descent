from pcoptim import LeastSquares, Logistic, SparseLogistic, SparseLeastSquares
from pcoptim import L1Regularizer, L2Regularizer, ElasticNetRegularizer

losses_desc = [
    (SparseLogistic, L1Regularizer, "dorothea", "raw", 0.01, True),
    (SparseLogistic, L2Regularizer, "dorothea", "raw", 1, True),
    (LeastSquares, L2Regularizer, "mtp", "norm", 5e-8, False),
    (LeastSquares, L1Regularizer, "lasso", "norm", 30, False),
    (LeastSquares, L1Regularizer, "california", "norm", 0.1, False),
    (Logistic, L2Regularizer, "lognormal_opt_1", "norm", 1.0/1000, False),
    (Logistic, L2Regularizer, "lognormal_opt_2", "norm", 1.0/1000, False),
    (SparseLogistic, L1Regularizer, "dexter", "norm", 0.1, True),
    (Logistic, L1Regularizer, "madelon", "norm", 0.05, False),
    (Logistic, L2Regularizer, "madelon", "norm", 1.0, False),
    #(SparseLogistic, L1Regularizer, "very_large_sparse", "raw", 0.0001, True),
    #(SparseLogistic, L2Regularizer, "very_large_sparse", "raw", 1.0/10000, True),
    (SparseLeastSquares, L1Regularizer, "large_sparse", "raw", 0.02, True), #use this one next
    #(SparseLeastSquares, L1Regularizer, "large_sparse", "raw", 0.1, True), #legacy
    (SparseLeastSquares, L2Regularizer, "large_sparse", "raw", 1.0/1000, True),
    (SparseLeastSquares, L1Regularizer, "square_sparse", "raw", 0.02, True), #use this one next
    #(SparseLeastSquares, L1Regularizer, "square_sparse", "raw", 0.1, True), #legacy
    (SparseLeastSquares, L2Regularizer, "square_sparse", "raw", 1.0/1000, True)
]
