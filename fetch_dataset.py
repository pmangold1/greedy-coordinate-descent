import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import scipy.sparse as sp
import pandas as pd
import os
import wget


from pydaha.visualization.optimals import optimal_objective
from pcoptim import Logistic
from sklearn.preprocessing import StandardScaler

from sklearn.datasets import fetch_california_housing
from sklearn.datasets import fetch_openml

from pydaha.datasets.normal import make_regression, make_classification


def save(directory, name, X, y, raw=True, norm=True, plot=True, sparse=False):
    # create directory
    os.makedirs(directory, exist_ok=True)
    path = directory + "/" + name

    # raw dataset
    if not sparse:
        X = np.array(X)
        X.dump(path + "_raw.data")
    else:
        X = sp.csc_matrix(X)
        sp.save_npz(path + "_raw.npz", X)

    y = np.array(y)
    y.dump(path + ".labels")

#    df = pd.DataFrame(X)
#    df["target"] = y
#    df.to_csv(path + "_raw.csv", index=False)

    # normalized dataset


    if not sparse:
        scaler = StandardScaler()
        X_norm = scaler.fit_transform(X)
        X_norm.dump(path + "_norm.data")
    else:
        scaler = StandardScaler(with_mean=False)
        X_norm = scaler.fit_transform(X)
        sp.save_npz(path + "_norm.npz", X_norm)

def fetch_dorothea():

    train_X_url = "https://archive.ics.uci.edu/ml/machine-learning-databases/dorothea/DOROTHEA/dorothea_train.data"
    train_y_url = "https://archive.ics.uci.edu/ml/machine-learning-databases/dorothea/DOROTHEA/dorothea_train.labels"

    wget.download(train_X_url, "datasets/dorothea/tmp/X.data")
    wget.download(train_y_url, "datasets/dorothea/tmp/y.data")

    X = np.zeros((800, 100000))
    y = np.zeros(800)

    with open("datasets/dorothea/tmp/X.data", "r") as f:
        for i, l in enumerate(f):
            elts = list(map(int, l.rstrip().split(" ")))
            for j in elts:
                X[i, j-1] = 1

    X = X[:, np.sum(X, axis=0) > 0]
    X = sp.csc_matrix(X)

    with open("datasets/dorothea/tmp/y.data", "r") as f:
        for i, l in enumerate(f):
            y[i] = int(l.rstrip())

    save("datasets/dorothea", "dorothea", X, y, sparse=True)

def fetch_california():
    X_california, y_california = fetch_california_housing(return_X_y=True)
    save("datasets/california", "california", X_california, y_california)


def fetch_sparse_lasso(n=1000, seed=42):
    rng = np.random.default_rng(seed=seed)
    w = np.zeros(n)
    w[:10] = rng.normal(scale=100, size=10)
    X_lasso, y_lasso, w_lasso = make_regression(n, n, w, seed=seed, snr=3)

    save("datasets/lasso", "lasso", X_lasso, y_lasso)


def fetch_indep_lognormal_opt(n=1000, p=100, seed=42, sigma=1):

    rng = np.random.default_rng(seed=seed)
    w = np.random.lognormal(sigma=sigma, size=p)
    w *= 100 / max(w)
    X, y, _ = make_classification(n, p, w, seed=seed, flip=0.1)

    save("datasets/lognormal_opt_" + str(sigma), "lognormal_opt_" + str(sigma), X, y)


def fetch_mtp():
    X_mtp, y_mtp = fetch_openml("mtp", return_X_y=True)
    X_mtp = np.array(X_mtp)

    y_mtp = np.array(y_mtp)

    save("datasets/mtp", "mtp", X_mtp, y_mtp)

def fetch_madelon():
    X_madelon, y_madelon = fetch_openml("madelon", return_X_y=True)

    y_madelon = np.array(y_madelon)
    y_madelon[y_madelon == '2'] = 1
    y_madelon[y_madelon == '1'] = -1

    save("datasets/madelon", "madelon", X_madelon, y_madelon)





fetch_dorothea()
fetch_california()
fetch_sparse_lasso(n=1000, seed=42)
fetch_indep_lognormal_opt(n=1000, p=100, seed=42, sigma=1)
fetch_indep_lognormal_opt(n=1000, p=100, seed=42, sigma=2)
fetch_mtp()
fetch_madelon()
